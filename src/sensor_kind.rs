use serde::Deserialize;
use strum::Display;

/// A list of sensors this crate supports.
#[derive(Clone, Copy, Debug, Deserialize, Display)]
pub enum SensorKind {
	Bme280,
	Mpu9250,
	Shtc3,
	Tsl2561,
}

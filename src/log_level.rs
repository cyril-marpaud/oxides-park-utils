use log::LevelFilter;
use serde::{Deserialize, Serialize};

///
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub enum LogLevel {
	Info,
	Warn,
	#[default]
	Error,
	/// No logs from OxidESPark
	Off,
	/// No logs at all
	SystemOff,
}

impl LogLevel {
	pub fn get_target(&self) -> &str {
		match self {
			LogLevel::SystemOff => "*",
			_ => "main",
		}
	}
}

impl From<LogLevel> for LevelFilter {
	fn from(log_level: LogLevel) -> Self {
		match log_level {
			LogLevel::Info => LevelFilter::Info,
			LogLevel::Warn => LevelFilter::Warn,
			LogLevel::Error => LevelFilter::Error,
			LogLevel::Off | LogLevel::SystemOff => LevelFilter::Off,
		}
	}
}

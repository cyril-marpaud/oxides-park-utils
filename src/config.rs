use std::{collections::HashMap, net::Ipv4Addr};

use config::{ConfigError, File, FileFormat};
use embedded_svc::wifi::AuthMethod;
use once_cell::sync::OnceCell;
use serde::Deserialize;

use crate::{log_level::LogLevel, measure::MeasureKind, sensor_kind::SensorKind};

pub static CONFIG_FILE: OnceCell<&str> = OnceCell::new();

#[derive(Debug, Deserialize)]
pub struct Config {
	pub esp: EspConfig,
	pub mqtt: Option<MqttConfig>,
	#[serde(default)]
	pub sensors: Vec<SensorConfig>,
	pub wifi: Option<WifiConfig>,
}

impl Config {
	/// Checks configuration file validity.
	///
	/// ```
	/// use oxide_spark_utils::config::Config;
	///
	/// let config = r#"
	///     [esp]
	///     id = "esp1"
	///     log_level = "Error"
	/// "#;
	///
	/// let erroneous_config = r#"
	///     [esp]
	///     id = "esp1"
	///     log_level = "Unknown"
	/// "#;
	///
	/// assert!(matches!(Config::check(config), Ok(())));
	/// assert!(matches!(Config::check(erroneous_config), Err(_)));
	/// ```
	pub fn check(config: &str) -> Result<(), ConfigError> {
		config::Config::builder()
			.add_source(File::from_str(config, FileFormat::Toml))
			.build()
			.expect("Failed to build configuration")
			.try_deserialize::<Config>()?;

		Ok(())
	}

	/// Sets the given configuration file. This should not be called manually.
	pub fn set(file: &'static str) {
		CONFIG_FILE
			.set(file)
			.expect("Configuration file has already been set")
	}
}

/// ESP-related configuration.
///
/// Example:
///
/// ```toml
/// [esp]
/// id = "esp1"
/// log_level = "Info"
/// ```
#[derive(Debug, Deserialize)]
pub struct EspConfig {
	/// The ID of the board
	pub id: String,
	#[serde(default)]
	/// Optional maximum level to log, defaults to "Error"
	pub log_level: LogLevel,
}

/// Optional WiFi-related configuration.
///
/// example:
///
/// ```toml
/// [wifi]
/// auth_method = "WPA2Personal"
/// ssid = "wifi_name"
/// pwd = "wifi_password"
/// timeout = 10
/// ```
#[derive(Debug, Deserialize)]
pub struct WifiConfig {
	#[serde(default)]
	// Optional authentication method, defaults to "WPA2Personal"
	pub auth_method: AuthMethod,
	pub pwd: String,
	pub ssid: String,
	/// Optional WiFi connection timeout in seconds, defaults to 10
	#[serde(default = "timeout")]
	pub timeout: u64,
}

/// Optional MQTT-related configuration.
///
/// example:
///
/// ```toml
/// [mqtt]
/// channel_size = 50
/// ip = "1.2.3.4"
/// port = 1883
#[derive(Debug, Deserialize)]
pub struct MqttConfig {
	/// Optional channel size, defaults to 50
	#[serde(default = "channel_size")]
	pub channel_size: usize,
	pub ip: Ipv4Addr,
	/// Optional MQTT port, defaults to 1883
	#[serde(default = "port")]
	pub port: u16,
}

/// Optional sensor-related configuration.
///
/// ```toml
/// [[sensors]]
/// freq = 5
/// id = "temp_hum_sensor"
/// kind = "Shtc3"
/// metrics = { temperature = "t", humidity = "h" }
/// ```
///
/// As denoted by the double square brackets, multiple sensors can be configured.
/// Simply add as much `[[sensors]]` sections as needed.
#[derive(Clone, Debug, Deserialize)]
pub struct SensorConfig {
	/// Optional sampling frequency in Hertz, defaults to 1
	#[serde(default = "freq")]
	pub freq: u32,
	pub id: String,
	pub kind: SensorKind,
	pub metrics: HashMap<MeasureKind, String>,
}

fn channel_size() -> usize {
	50
}

fn freq() -> u32 {
	1
}

fn port() -> u16 {
	1883
}

fn timeout() -> u64 {
	10
}

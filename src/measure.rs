use std::fmt::Display;

use serde::Deserialize;
use strum::EnumDiscriminants;

/// A list of measure kinds OxidESPark supports.
#[derive(Clone, Copy, EnumDiscriminants)]
#[strum_discriminants(derive(Deserialize, strum::Display, Hash), name(MeasureKind))]
pub enum Measure {
	Acceleration(f32, f32, f32),
	Gyroscope(f32, f32, f32),
	Humidity(f32),
	InfraredLight(f32),
	Pressure(f32),
	Temperature(f32),
	VisibleInfraredLight(f32),
}

impl Display for Measure {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"{}",
			match self {
				Measure::Acceleration(x, y, z) | Measure::Gyroscope(x, y, z) =>
					format!("{}_{}_{}", x, y, z),
				Measure::Humidity(m)
				| Measure::InfraredLight(m)
				| Measure::Pressure(m)
				| Measure::Temperature(m)
				| Measure::VisibleInfraredLight(m) => format!("{}", m),
			}
		)
	}
}

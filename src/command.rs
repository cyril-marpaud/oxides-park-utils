pub mod led_strip_mode;
pub mod system_command;

use led_strip_mode::LedStripMode;
use serde::{Deserialize, Serialize};
use system_command::SystemCommand;

#[non_exhaustive]
#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub enum Command {
	LedStrip(LedStripMode),
	System(SystemCommand),
}

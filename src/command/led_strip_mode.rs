use serde::{Deserialize, Serialize};
use smart_leds::{colors::WHITE, RGB8};

#[non_exhaustive]
#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub enum LedStripMode {
	Full(RGB8),
	Rainbow(Direction),
	Snake(SnakeMode, Direction),
}

impl Default for LedStripMode {
	fn default() -> Self {
		Self::Snake(SnakeMode::Classic(WHITE), Default::default())
	}
}

#[derive(Clone, Copy, Debug, Default, Deserialize, Serialize)]
pub enum Direction {
	Left,
	#[default]
	Right,
}

#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub enum SnakeMode {
	Classic(RGB8),
	Rainbow,
}

impl Default for SnakeMode {
	fn default() -> Self {
		Self::Classic(Default::default())
	}
}

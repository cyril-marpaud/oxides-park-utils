# OxidESParkUtils

OxidESParkUtils is an [OxidESPark](https://gitlab.com/cyril-marpaud/oxide-spark) dependency which includes what does not depend on the [`esp-idf-sys`](https://crates.io/crates/esp-idf-sys) crate.

As the `esp-idf-sys` crate can only be compiled for the `riscv32imc-esp-espidf` target, this crate mainly exists to allow command serialization from crates targeting other architectures (e.g. `x86_64-unknown-linux-gnu`).

See the [OxidESPark](https://gitlab.com/cyril-marpaud/oxide-spark) documentation for more information.
